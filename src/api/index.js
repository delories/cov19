import {get} from '../utils/request'
const api={
    getDataInfo(params){
        return get("http://api.tianapi.com/ncov/index",params);
    },
    getProvienceInfo(){
        return get("https://route.showapi.com/2217-2?showapi_appid=979222.0&showapi_sign=5731ccc92a464302b098963febda4c50");
    },
    getTravelPolicy(params){
        return get("http://apis.juhe.cn/springTravel/query",params);
    },
    getCityId(params){
        return get("https://v2.alapi.cn/api/springTravel/city",params);
    }
}
export default api;