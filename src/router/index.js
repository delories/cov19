import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
//import CityView from '../views/CityView'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta:{
        keepAlive:true,//是否缓存组件
        //useCache:true//是否用缓存
    }
  },
  {
    path:'/provienceMap/:name',
    name:'provience',
    //异步加载省信息路由，减少首屏渲染时间。
    component:()=> import(/*webpackChunkName:CityView*/ '../views/CityView.vue'),
  }
]

const router = new VueRouter({
  mode:'hash',
  routes
})

export default router
