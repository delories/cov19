import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const moduleNums={
    state:{},
    mutations:{},
    getters:{

    },
    actions:{

    }
}
const store=new Vuex.Store({
    modules:{
        a:moduleNums
    },
})

export default store;