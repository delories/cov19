const { defineConfig } = require('@vue/cli-service');
const CompressionWebpackPlugin = require('compression-webpack-plugin');
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin')
const MiniCssExtractPlugin=require('mini-css-extract-plugin')
//const TerserPlugin = require("terser-webpack-plugin");
//const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const LodashModuleReplacementPlugin = require("lodash-webpack-plugin");
/*const path = require('path');

const webpack = require('webpack')
const CompressionPlugin = require('compression-webpack-plugin')
const zlib = require('zlib')*/
//const isProduction = process.env.NODE_ENV === 'production'

module.exports = defineConfig({
  transpileDependencies: true,
  chainWebpack:config=>{
  /*let miniCssExtractPlugin = new MiniCssExtractPlugin({
    filename: 'assets/[name].[hash:8].css',
    chunkFilename: 'assets/[name].[hash:8].css'
  })
  config.plugin('extract-css').use(miniCssExtractPlugin);
  config.plugin("loadshReplace")
    .use(new LodashModuleReplacementPlugin());*/
  config.optimization.minimizer('terser').tap((args) => {
      args[0].terserOptions.compress.drop_console = false
      return args
    });
  /*let miniCssExtractPlugin = new MiniCssExtractPlugin({
    filename: 'assets/[name].[hash:8].css',
    chunkFilename: 'assets/[name].[hash:8].css'
  })*/
    /*
  config
      .plugin('webpack-bundle-analyzer')
      .use(BundleAnalyzerPlugin);*/
  config
      .plugin('speed-measure-webpack-plugin')
      .use(new SpeedMeasurePlugin())
      /*config.plugin('webpack-bundle-analyzer')
      .use(new BundleAnalyzerPlugin())
      config.plugin('extract-css').
      use(miniCssExtractPlugin)
      config.plugin("loadshReplace")
    .use(new LodashModuleReplacementPlugin())
      .end();*/


  },
  productionSourceMap: false,
  configureWebpack: config => {
        if (process.env.NODE_ENV === 'production') {
            config.plugins.push(new CompressionWebpackPlugin({
                        filename: '[path][name].gz',
                        test: /\.(js|css)$/i
                    }));
                    /*new UglifyJsPlugin({
                        uglifyOptions: {
                            compress: {
                            //warnings: false,
                            drop_console: true,  //注释console
                            //drop_debugger: true, //注释debugger
                            pure_funcs: ['console.log'], //移除console.log
                            },
                        },
                    }),*/
            }
      // 为生产环境修改配置...

    }
})
//const isProduction = process.env.NODE_ENV === 'production'

